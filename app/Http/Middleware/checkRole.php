<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Post; 

class checkRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $role)
    {
        $roles = collect(explode('|',$role));

        foreach ($roles as $role) {

            if($role == "Author"){
                if($request->user('api')->id == Post::findOrFail($request->route()->parameter('post_id'))->author_id){
                    return $next($request);
                }
                else {
                    return "Unauthorized"; 
                }
            }
            else if($request->user('api')->role == $role) {
                return $next($request);
            }
        }
        return ("Unauthorized");
    
    }

}
